package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.PatientRegisterForm;
import co.com.proyectobase.screenplay.questions.CorrectMensage;
import co.com.proyectobase.screenplay.tasks.FillPatient;
import co.com.proyectobase.screenplay.tasks.RegisterPatient;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class PatientRecord {

	@Managed (driver="chrome")
	private WebDriver hisBrowser;
	
	private Actor cyan = Actor.named("Cyan"); 
	
	@Before 
	public void initialConfiguration(){
		cyan.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^violet needs to register a new patient$")
	public void violetNeedsToRegisterANewPatient() throws Exception {
		cyan.wasAbleTo(RegisterPatient.newPatient());
	}


	@When("^she registers a new patient in the Hospital Administration application$")
	public void sheRegistersANewPatientInTheHospitalAdministrationApplication(List<PatientRegisterForm> datesPatient) throws Exception {
	    cyan.attemptsTo(FillPatient.formTwo(datesPatient));
	}

	@Then("^she verifies the message for the new patient (.*) appears on the screen$")
	public void sheVerifiesTheMessageForTheNewPatientDataSavedCorrectlyAppearsOnTheScreen(String expectedMensage) throws Exception {
		cyan.should(GivenWhenThen.seeThat(CorrectMensage.savePatient(), Matchers.equalTo(expectedMensage)));
	    
	}
	
}
