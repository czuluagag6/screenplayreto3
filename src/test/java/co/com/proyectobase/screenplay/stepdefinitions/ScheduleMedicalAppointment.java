package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.ScheduleRegisterForm;
import co.com.proyectobase.screenplay.questions.CorrectFinalAnswer;
import co.com.proyectobase.screenplay.tasks.FillSchedule;
import co.com.proyectobase.screenplay.tasks.Scheduling;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class ScheduleMedicalAppointment {
	
	@Managed (driver="chrome")
	private WebDriver hisBrowser;
	
	private Actor cyan = Actor.named("Cyan"); 
	
	@Before 
	public void initialConfiguration(){
		cyan.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^that Clark needs to see the doctor$")
	public void thatClarkNeedsToSeeTheDoctor() throws Exception {
		cyan.wasAbleTo(Scheduling.MedicalAppointment());
	}


	@When("^he performs the scheduling of a medical appointment$")
	public void hePerformsTheSchedulingOfAMedicalAppointment(List<ScheduleRegisterForm> datesSchedule) throws Exception {
		cyan.attemptsTo(FillSchedule.formThree(datesSchedule));
	   
	}

	@Then("^he verifies that the message (.*) appears on the screen$")
	public void heVerifiesThatTheMessageDataSavedCorrectlyAppearsOnTheScreen(String expectedMessage) throws Exception {
	    cyan.should(GivenWhenThen.seeThat(CorrectFinalAnswer.FinalMessage(), Matchers.equalTo(expectedMessage)));
	    
	}
}
