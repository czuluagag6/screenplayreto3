package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.DoctorRegister;
import co.com.proyectobase.screenplay.questions.Answer;
import co.com.proyectobase.screenplay.tasks.Fill;
import co.com.proyectobase.screenplay.tasks.Register;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class DoctorRecord {

	@Managed (driver="chrome")
	private WebDriver hisBrowser;
	
	private Actor cyan = Actor.named("Cyan"); 
	
	@Before 
	public void initialConfiguration(){
		cyan.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^that violet needs to register a new doctor$")
	public void thatVioletNeedsToRegisterANewDoctor() throws Exception {
		cyan.wasAbleTo(Register.newDoctor());
	}


	@When("^she registers it in the Hospital Administration application$")
	public void sheRegistersItInTheHospitalAdministrationApplication(List<DoctorRegister> datesDoctor) throws Exception {
	    cyan.attemptsTo(Fill.formOne(datesDoctor));
	}

	@Then("^she verifies that the message (.*) appears on the screen$")
	public void sheVerifiesThatTheMessageDataSavedCorrectlyAppearsOnTheScreen(String expectedMessage) throws Exception {
	    cyan.should(GivenWhenThen.seeThat(Answer.correct(), Matchers.equalTo(expectedMessage)));
	}
	
}
