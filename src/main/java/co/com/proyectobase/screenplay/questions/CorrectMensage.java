package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.userinterface.FormTwoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class CorrectMensage implements Question<String>{

	public static CorrectMensage savePatient() {
		return new CorrectMensage();
	}

	@Override
	public String answeredBy(Actor actor) {
		return Text.of(FormTwoPage.MENSAGE_CORRECT).viewedBy(actor).asString();
	}

}
