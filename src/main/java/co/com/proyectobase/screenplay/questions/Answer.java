package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.userinterface.FormOnePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Answer implements Question<String>{

	public static Answer correct() {
		return new Answer();
	}

	@Override
	public String answeredBy(Actor actor) {
		return Text.of(FormOnePage.EXPECTED_MESSAGE).viewedBy(actor).asString();
	}

}
