package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.userinterface.FormThree;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class CorrectFinalAnswer implements Question<String>{

	
	@Override
	public String answeredBy(Actor actor) {
		
		return Text.of(FormThree.FINAL_MESAGGE).viewedBy(actor).asString();
	}

	public static CorrectFinalAnswer FinalMessage() {
		
		return new CorrectFinalAnswer();
	}

}
