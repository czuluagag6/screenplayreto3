package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.model.ScheduleRegisterForm;
import co.com.proyectobase.screenplay.userinterface.FormThree;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class FillSchedule implements Task{
		
	private List<ScheduleRegisterForm> datesScheduleForm;

	public FillSchedule (List<ScheduleRegisterForm> datesScheduleForm) {
		super();
		this.datesScheduleForm = datesScheduleForm;
	}
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(datesScheduleForm.get(0).getDateOfTheAppointment()).into(FormThree.DATE_APPOINTMENT));
		actor.attemptsTo(Click.on(FormThree.CLICK_OUT));
		actor.attemptsTo(Enter.theValue(datesScheduleForm.get(0).getPatientIdentityDocument()).into(FormThree.PATIENT_IDENTITY_DOCUMENT));
		actor.attemptsTo(Enter.theValue(datesScheduleForm.get(0).getDoctorIdentityDocument()).into(FormThree.DOCTOR_IDENTITY_DOCUMENT));
		actor.attemptsTo(Enter.theValue(datesScheduleForm.get(0).getObservations()).into(FormThree.OBSERVATIONS));
		actor.attemptsTo(Click.on(FormThree.BUTTON_SCHEDULE_SAVE));
		try {
			Thread.sleep(10000);
		}
		catch(InterruptedException e) {
		}
	}
	
	public static FillSchedule formThree(List<ScheduleRegisterForm> datesSchedule) {
		return Tasks.instrumented(FillSchedule.class, datesSchedule);
	}

}
