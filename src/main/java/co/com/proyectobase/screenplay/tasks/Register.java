package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.InitialPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class Register implements Task{

	private InitialPage initialPage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(initialPage));
		actor.attemptsTo(Click.on(InitialPage.DOCTOR_BUTTON_ADD));
	}

	public static Register newDoctor() {
		
		return Tasks.instrumented(Register.class);
	}


}
