package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.model.DoctorRegister;
import co.com.proyectobase.screenplay.userinterface.FormOnePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Fill implements Task{

	private List<DoctorRegister> doctorRegister;
	
	public Fill(List<DoctorRegister> doctorRegister) {
		super();
		this.doctorRegister = doctorRegister;
	}
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(doctorRegister.get(0).getName()).into(FormOnePage.DOCTOR_NAME));
		actor.attemptsTo(Enter.theValue(doctorRegister.get(0).getLastName()).into(FormOnePage.DOCTOR_LAST_NAME));
		actor.attemptsTo(Enter.theValue(doctorRegister.get(0).getPhone()).into(FormOnePage.DOCTOR_PHONE));
		actor.attemptsTo(SelectFromOptions.byVisibleText(doctorRegister.get(0).getIdentificationType()).from(FormOnePage.IDENTIFICATION_TYPE));
		actor.attemptsTo(Enter.theValue(doctorRegister.get(0).getIdentificationNumber()).into(FormOnePage.IDENTIFICATION_NUMBER));
		actor.attemptsTo(Click.on(FormOnePage.BUTTON_SAVE));
		try {
			Thread.sleep(5000);
		}
		catch(InterruptedException e) {
		}	
		
	}

	
	
	
	public static Fill formOne(List<DoctorRegister> doctorRegister) {
		return Tasks.instrumented(Fill.class, doctorRegister);
	}
	

}
