package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.InitialPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class RegisterPatient implements Task{
	
	private InitialPage initialPage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(initialPage));
		actor.attemptsTo(Click.on(InitialPage.PATIENT_BUTTON_ADD));
		
	}
	
	public static RegisterPatient newPatient() {
		
		return Tasks.instrumented(RegisterPatient.class);
	}

}
