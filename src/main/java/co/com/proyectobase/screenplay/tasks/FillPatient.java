package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.model.PatientRegisterForm;
import co.com.proyectobase.screenplay.userinterface.FormTwoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class FillPatient implements Task {
	
	private List<PatientRegisterForm> patientRegisterForm;
	
	public FillPatient (List<PatientRegisterForm> patientRegisterForm) {
		super();
		this.patientRegisterForm = patientRegisterForm;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Enter.theValue(patientRegisterForm.get(0).getNamePatient()).into(FormTwoPage.PATIENT_NAME));
		actor.attemptsTo(Enter.theValue(patientRegisterForm.get(0).getLastNamePatient()).into(FormTwoPage.PATIENT_LAST_NAME));
		actor.attemptsTo(Enter.theValue(patientRegisterForm.get(0).getPhonePatient()).into(FormTwoPage.PATIENT_PHONE));
		actor.attemptsTo(SelectFromOptions.byVisibleText(patientRegisterForm.get(0).getIdentificationTypePatient()).from(FormTwoPage.IDENTIFICATION_TYPE_PATIENT));
		actor.attemptsTo(Enter.theValue(patientRegisterForm.get(0).getIdentificationNumberPatient()).into(FormTwoPage.IDENTIFICATION_NUMBER_PATIENT));
		actor.attemptsTo(Click.on(FormTwoPage.BUTTON_PREPAID_HEALT));
		actor.attemptsTo(Click.on(FormTwoPage.BUTTON_PATIENT_SAVE));
		try {
			Thread.sleep(5000);
		}
		catch(InterruptedException e) {
		}	
		
	}
	
	public static FillPatient formTwo(List<PatientRegisterForm> patientRegister) {
		return Tasks.instrumented(FillPatient.class, patientRegister);
	}

}
