package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.InitialPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class Scheduling implements Task{
	
	private InitialPage initialPage; 

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(initialPage));
		actor.attemptsTo(Click.on(InitialPage.SCHEDULE_MEDICAL_APPOINTMENT));
	
	}
	
	public static Scheduling MedicalAppointment() {
		
		return Tasks.instrumented(Scheduling.class);
	}

}
