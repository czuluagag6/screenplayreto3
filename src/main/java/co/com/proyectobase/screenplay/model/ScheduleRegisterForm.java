package co.com.proyectobase.screenplay.model;

public class ScheduleRegisterForm {
	private String dateOfTheAppointment;
	private String patientIdentityDocument;
	private String doctorIdentityDocument;
	private String observations;

	public String getDateOfTheAppointment() {
		return dateOfTheAppointment;
	}

	public void setDateOfTheAppointment(String dateOfTheAppointment) {
		this.dateOfTheAppointment = dateOfTheAppointment;
	}

	public String getPatientIdentityDocument() {
		return patientIdentityDocument;
	}

	public void setPatientIdentityDocument(String patientIdentityDocument) {
		this.patientIdentityDocument = patientIdentityDocument;
	}

	public String getDoctorIdentityDocument() {
		return doctorIdentityDocument;
	}

	public void setDoctorIdentityDocument(String doctorIdentityDocument) {
		this.doctorIdentityDocument = doctorIdentityDocument;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}
	
}
