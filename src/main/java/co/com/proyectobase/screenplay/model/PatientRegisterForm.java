package co.com.proyectobase.screenplay.model;

public class PatientRegisterForm {
	private String namePatient;
	private String lastNamePatient;
	private String phonePatient;
	private String identificationTypePatient;
	private String identificationNumberPatient;

	public String getNamePatient() {
		return namePatient;
	}

	public void setNamePatient(String namePatient) {
		this.namePatient = namePatient;
	}

	public String getLastNamePatient() {
		return lastNamePatient;
	}

	public void setLastNamePatient(String lastNamePatient) {
		this.lastNamePatient = lastNamePatient;
	}

	public String getPhonePatient() {
		return phonePatient;
	}

	public void setPhonePatient(String phonePatient) {
		this.phonePatient = phonePatient;
	}

	public String getIdentificationTypePatient() {
		return identificationTypePatient;
	}

	public void setIdentificationTypePatient(String identificationTypePatient) {
		this.identificationTypePatient = identificationTypePatient;
	}

	public String getIdentificationNumberPatient() {
		return identificationNumberPatient;
	}

	public void setIdentificationNumberPatient(String identificationNumberPatient) {
		this.identificationNumberPatient = identificationNumberPatient;
	}

}
