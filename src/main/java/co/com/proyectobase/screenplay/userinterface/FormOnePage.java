package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class FormOnePage extends PageObject{

	public static final Target DOCTOR_NAME = Target.the("DOCTOR_NAME").located(By.id("name"));
	public static final Target DOCTOR_LAST_NAME = Target.the("DOCTOR_LAST_NAME").located(By.id("last_name"));
	public static final Target DOCTOR_PHONE = Target.the("DOCTOR_PHONE").located(By.id("telephone"));
	public static final Target IDENTIFICATION_TYPE = Target.the("IDENTIFICATION_TYPE").located(By.id("identification_type"));
	public static final Target IDENTIFICATION_NUMBER = Target.the("IDENTIFICATION_NUMBER").located(By.id("identification"));
	public static final Target BUTTON_SAVE = Target.the("BUTTON_SAVE").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/a"));
	public static final Target EXPECTED_MESSAGE = Target.the("EXPECTED_MESSAGE").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div[1]/h3"));
	
}
