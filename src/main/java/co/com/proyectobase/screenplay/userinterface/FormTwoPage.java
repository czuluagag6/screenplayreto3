package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class FormTwoPage extends PageObject{

	public static final Target PATIENT_NAME = Target.the("PATIENT_NAME").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[1]/input"));
	public static final Target PATIENT_LAST_NAME = Target.the("PATIENT_LAST_NAME").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[2]/input"));
	public static final Target PATIENT_PHONE = Target.the("PATIENT_PHONE").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[3]/input"));
	public static final Target IDENTIFICATION_TYPE_PATIENT = Target.the("IDENTIFICATION_TYPE_PATIENT").located(By.name("identification_type"));
	public static final Target IDENTIFICATION_NUMBER_PATIENT = Target.the("IDENTIFICATION_NUMBER_PATIENT").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[5]/input"));
	public static final Target BUTTON_PREPAID_HEALT = Target.the("BUTTON_PREPAID_HEALT").located(By.name("prepaid"));
	public static final Target BUTTON_PATIENT_SAVE = Target.the("BUTTON_PATIENT_SAVE").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/a"));
	public static final Target MENSAGE_CORRECT = Target.the("MENSAGE_CORRECT").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div[2]/p"));
	
}
