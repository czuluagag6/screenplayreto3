package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")

public class InitialPage extends PageObject{

	public static final Target DOCTOR_BUTTON_ADD = Target.the("DOCTOR_BUTTON_ADD").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[1]"));
	public static final Target PATIENT_BUTTON_ADD = Target.the("PATIENT_BUTTON_ADD").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[2]"));
	public static final Target SCHEDULE_MEDICAL_APPOINTMENT = Target.the("SCHEDULE_MEDICAL_APPOINTMENT").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[6]"));

}
