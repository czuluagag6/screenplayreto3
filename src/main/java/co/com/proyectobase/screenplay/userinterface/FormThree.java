package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class FormThree extends PageObject{
	
	public static final Target DATE_APPOINTMENT = Target.the("DATE_APPOINTMENT").located(By.id("datepicker"));
	public static final Target CLICK_OUT = Target.the("CLICK_OUT").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[1]/div/h1"));
	public static final Target PATIENT_IDENTITY_DOCUMENT = Target.the("PATIENT_IDENTITY_DOCUMENT").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[2]/input"));
	public static final Target DOCTOR_IDENTITY_DOCUMENT = Target.the("DOCTOR_IDENTITY_DOCUMENT").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[3]/input"));
	public static final Target OBSERVATIONS = Target.the("OBSERVATIONS").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[4]/textarea"));
	public static final Target BUTTON_SCHEDULE_SAVE = Target.the("BUTTON_SCHEDULE_SAVE").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/a"));
	public static final Target FINAL_MESAGGE = Target.the("FINAL_MESAGGE").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div[2]/p"));	
}
