#Author: czuluagag@choucairtesting.com

@tag
Feature: Hospital administration system

  @scenarioOne
  Scenario: Perform a Doctors Record
    Given that violet needs to register a new doctor
    When she registers it in the Hospital Administration application
    |name			|lastName	|phone			|identificationType    |identificationNumber  |
    |Stephen 	|Strange	|312666999	|Cédula de extrangería |66699966699911					|
    Then she verifies that the message Guardado: appears on the screen
   
	@scenarioTwo
	Scenario: Perform a Patient Registration
		Given violet needs to register a new patient
		When she registers a new patient in the Hospital Administration application
		|namePatient	|lastNamePatient	|phonePatient			|identificationTypePatient    |identificationNumberPatient  |
		|Clark				|Kent							|310666888				|Pasaportes										|5556661016										|
		Then she verifies the message for the new patient Datos guardados correctamente. appears on the screen
	
	@scenarioThree
	Scenario: Schedule a medical appointment
		Given that Clark needs to see the doctor
		When he performs the scheduling of a medical appointment
		|dateOfTheAppointment|patientIdentityDocument|doctorIdentityDocument	|observations				|
		|10/04/2018					 |5556661016						 |66699966699911 					|Recurring headache	|
		Then he verifies that the message Datos guardados correctamente. appears on the screen 